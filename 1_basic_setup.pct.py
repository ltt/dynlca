# ---
# jupyter:
#   jupytext:
#     formats: ipynb,.pct.py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.4.2
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Basic Setup
# In this notebook, we create a new project and import ecoinvent 3.5 with the apos system model.

# %% [markdown]
# Check your current version of important python packages. This code was written with Brightway 2.3, bw2io 0.7.11.3 and wurst 0.2.

# %%
import brightway2 as bw

print("brightway2", bw.__version__)

import bw2io

print("bw2io", bw2io.__version__)

import wurst

print("wurst", wurst.__version__)

# %% [markdown]
# ## Choosing the project
# Prepare Brightway2. First, check which projects are available.

# %%
bw.projects.report()

# %% [markdown]
# <div class="alert alert-info">
#
# Note
#
# Change the default project name here. You will also need to specify this project name
# in [2_creating_new_versions.ipynb](2_creating_new_versions.ipynb).
#
# You can choose any of the already existing projects or create your own new project.
# If you are using an already existing project, please make sure that you are not overwriting any important data.
#
# </div>

# %%
bw.projects.set_current("your_project")
bw.databases

# %% [markdown]
# ## Setting up Brightway2
# First, we need to do the basic setup of Brightway2's biosphere database and the predefined methods.
# If the biosphere database exists already, we assume that the setup was already done.

# %%
if "biosphere3" not in bw.databases:
    bw.bw2setup()

# %% [markdown]
# Next, we import the ecoinvent database. We use version 3.5 of the apos system model. If you are using a different system model, change the database name accordingly.
#
# <div class="alert alert-info">
#
# Note
#
# You will need to change the path to the ecoinvent datasets folder!
#
# </div>

# %%
filepath = "data/ecoinvent 3.5_apos_ecoSpold02/datasets"

ei = bw.SingleOutputEcospold2Importer(filepath, "ecoinvent3.5apos")
ei.apply_strategies()
ei.statistics()
ei.write_database()

# %% [markdown]
# Check that the database has been created under the specified name:

# %%
bw.databases

# %%
bw.projects.report()

# %% [markdown]
# ## Parsing Scenario data
# Our ecoinvent modifications are based on the predicted technology composition
# (referred to as *scenarios*) from the IEA report
# [Energy Technology Perspectives 2017](https://www.iea.org/reports/energy-technology-perspectives-2017).
# To use the scenarios they provide, we first have to split up the summary table.
# We do this using [pandas](https://pandas.pydata.org/).

# %%
import pandas as pd
import os

filename = "data/ETP2017_scenario_summary.xlsx"
dfs = pd.read_excel(filename, sheet_name=None)

# %% [markdown]
# The imported Excel file contains some sheets that do not contain any data. We delete those here.

# %%
dfs.pop("Information", None)
dfs.pop("Graph", None)
scenarios = {}


# %% [markdown]
# The different scenarios are separated by empty columns and the different sectors are separated by empty rows,
# thus we define a function to help us split up a DataFrame by empty rows/columns.
#
# The functions inputs are a DataFrame ``df`` and a ``val_dict``, that maps the column/row indices of ``df`` to a boolean value.
# It will split ``df`` on the columns/rows that are marked ``False`` in ``val_dict``.
# Optionally, the ``axis`` parameter (default 1) can be used to switch between operations on columns (1) and rows (0).

# %%
def split_by_val(df, val_dict, axis=1):
    slc = lambda df, idx: df.loc[:, idx] if axis == 1 else df.iloc[idx]
    res = []
    cr_idx = []
    for i, val in val_dict.items():
        if val:
            cr_idx.append(i)
        elif cr_idx:
            res.append(slc(df, cr_idx))
            cr_idx = []
    if cr_idx:
        res.append(slc(df, cr_idx))
    return res


# %% [markdown]
# Now we split up all the DataFrames (sheets), first by empty rows and then by empty columns.
# This yields a new DataFrame for every combination of scenario, sector and region.
# We delete unneeded data and columns from those DataFrames and
# transform them to the same format originally used in
# [Mendoza Beltran et al (2018)](https://onlinelibrary.wiley.com/doi/full/10.1111/jiec.12825).
# Finally, we save them to a dictionary indexed with a tuple containing the scenario, sector and region.

# %% tags=[]
for country, df in dfs.items():
    scen_dfs = split_by_val(df, df.notna().any())

    for d in scen_dfs:
        name = next(n.split("-") for n in d.columns if "Unnamed" not in n)
        name = name[-1].strip()
        scens = split_by_val(d, ~df.isna().all(axis="columns"), axis=0)
        res = {}
        for i in scens[1:]:
            i_name = i.iat[0, 0]
            i = i.iloc[1:]
            i = i.set_index(i.columns[1])
            i = i.drop(columns=i.columns[0])
            i = i.drop(index=["Other", "Total"], errors="ignore")
            i.columns = scens[0].iloc[0].iloc[2:]
            i = i.rename_axis(None)
            scenarios[(name, i_name, country)] = i.T

# %% [markdown] jupyter={"outputs_hidden": true}
# The resulting DataFrames have a column for every technology listed in the respective sector
# and each row corresponds to the prediction made for a certain year.
#
# We can now export the DataFrames. We create an Excel file for every sector with a sheet
# containing the respective DataFrame for each country.

# %% tags=[]
scen_names = set(n for n, _, _ in scenarios)
sectors = set(s for _, s, _ in scenarios)
countries = set(c for _, _, c in scenarios)
for scen in scen_names:
    for sec in sectors:
        directory = "data/" + scen + "/"
        os.makedirs(directory, exist_ok=True)
        fpath = os.path.abspath(directory + sec + ".xlsx")
        with pd.ExcelWriter(fpath) as writer:
            for country, df in [
                (key[2], d)
                for key, d in scenarios.items()
                if key[0] == scen and key[1] == sec
            ]:
                df.to_excel(writer, sheet_name=country)

# %% [markdown]
# ## Preparing additional LCI
# The additional LCI's provided with Mendoza Beltran, A. et al. (2018) contain a summary sheet
# which conflicts with the Brightway2 ExcelImporter. We now remove those unneeded sheets.

# %%
import openpyxl

for orig, copy in {
    "data/5907amb2z_SI_lci-Carma-CCS.xlsx": "data/lci-Carma-CCS.xlsx",
    "data/5907amb2z_SI_lci-CSP.xlsx": "data/lci-CSP.xlsx",
}.items():
    book = openpyxl.load_workbook(orig)
    book.remove(book["Summary"])
    book.save(copy)


# %% [markdown]
# ## Generating Sign table
# In ecoinvent's official cumulated matrices, some processes are multiplied with -1.
# Those processes yield a credit in the LCA. To retain this information in our
# exported matrices export, we extract these signs from the official ecoinvent cumulated matrix table.
#
# <div class="alert alert-info">
#
# Note
#
# In the following, change the file path depending on where your copy of ecoinvent's cumulative LCIA is located.
# If the table does not generate properly, it is most likely due to the layout of the cumulative LCIA differing
# from the version we originally used. In this case, you will need to change some of the import parameters in the following cell.
# </div>

# %%
data = pd.read_excel(
    "data/v35_apos_cumulative_LCIA.xlsx",
    usecols="B:D",
    skiprows=[0, 1],
    names=["process", "unit", "sign"],
)

# %% [markdown]
# The next step is to match ecoinvent and wurst units. This is necessary because ecoinvent uses acronyms, while wurst uses the full name.

# %%
from functions_to_modify_ecoinvent import unit_ei_to_bw

data["unit"] = data["unit"].apply(lambda x: unit_ei_to_bw.get(x, x))

# %% [markdown]
# Export the sign table.

# %%
data.to_excel("data/ecoinvent_signs.xlsx", index=False)
