.. Citations

.. |IEA_Report| replace:: *Energy Technology Perspectives 2017*
.. _IEA_Report: https://www.iea.org/reports/energy-technology-perspectives-2017

.. |Mendoza| replace:: *When the Background Matters\: Using Scenarios from Integrated Assessment Models in Prospective Life Cycle Assessment*
.. _Mendoza: https://onlinelibrary.wiley.com/doi/full/10.1111/jiec.12825 
.. |Mendoza_cit| replace:: Mendoza Beltran, A. et al. (2018)
.. _Mendoza_cit: Mendoza_

.. |Reinert| replace:: *Environmental Impacts of the Future German Energy System from Integrated Energy Systems Optimization and Dynamic Life Cycle Assessment*
.. |Reinert_cit| replace:: Reinert, C. et al. (2021)
