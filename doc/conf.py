# Configuration file for the Sphinx documentation builder.

# -- Path setup -------------------------------------------------------------

import os
import sys

sys.path.insert(0, os.path.abspath(".."))


# -- Project information ----------------------------------------------------

project = "Creating new versions of ecoinvent"
copyright = "2020, Chair of Technical Thermodynamics, RWTH Aachen University"
author = "Chair of Technical Thermodynamics, RWTH Aachen University"

# The full version, including alpha/beta/rc tags
release = ""
# latex_elements = {"releasename": ""} is in latex options

# -- General configuration --------------------------------------------------

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "nbsphinx",
    "IPython.sphinxext.ipython_console_highlighting",
    "IPython.sphinxext.ipython_directive",
]

exclude_patterns = [
    "_build",
    "Thumbs.db",
    ".DS_Store",
    "**.ipynb_checkpoints",
    "*.ipynb",
    "doc/global.rst",
]

rst_prolog = open("global.rst", "r").read()

# -- Options for HTML output ------------------------------------------------

html_theme = "sphinx_rtd_theme"
html_theme_path = [
    "_themes",
]

# -- Options for LaTeX output -----------------------------------------------
latex_engine = "xelatex"
latex_elements = {
    "inputenc": "",
    "utf8extra": "",
    "releasename": "",
    "preamble": r"""

\usepackage{fontspec}
\setsansfont{Arial}
\setromanfont{Arial}
\setmonofont{DejaVu Sans Mono}
""",
}

# -- Extension Options ------------------------------------------------------
# nbsphinx
nbsphinx_execute = "never"

nbsphinx_custom_formats = {
    ".pct.py": ["jupytext.reads", {"fmt": "py:percent"}],
}

nbsphinx_prolog = r"""
{% set docname = env.doc2path(env.docname, base=None) %}
.. raw:: html

    <div class="admonition note">

    <p>This page was generated from {{ docname }}.
    </p>
    </div>

"""
