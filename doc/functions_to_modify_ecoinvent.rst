Functions
=========

.. py:currentmodule:: functions_to_modify_ecoinvent

As the code is largely based on Brightway2 and wurst,
we refer to all types of processes as *activities*
and to the flows between processes as *exchanges*,
to remain consistent with the terminology of these libraries.

Input parameters of many of the following functions are activities or a database of activities.
Activities and databases are always expected to be in the `data format specified in the wurst library
<https://wurst.readthedocs.io/#internal-data-format>`__.

Electricity activities
----------------------

.. autofunction:: update_electricity_markets_iea

.. autofunction:: add_new_datasets_to_electricity_market_iea

.. autofunction:: delete_electricity_inputs_from_market

.. autofunction:: add_new_locations_to_added_datasets_iea

.. autofunction:: regionalize_added_datasets

Searching
^^^^^^^^^

All functions reffering to geographical intersection internally use
constructive geometry's ``Gemoatcher.intersects`` functionality,
which is documented `as part of the docs of constructive_geometries
<https://constructive-geometries.readthedocs.io/?badge=latest#constructive_geometries.geomatcher.Geomatcher.intersects>`_.

.. autofunction:: find_ecoinvent_elec_ds_in_all_locations_iea

.. autofunction:: find_ecoinvent_elec_ds_in_iea_region

.. autofunction:: find_ecoinvent_elec_ds_in_same_ecoinvent_location_iea

.. autofunction:: find_other_ecoinvent_regions_in_iea_region

.. autofunction:: ecoinvent_to_iea_locations

Scenario handling
-----------------

.. autofunction:: interpolate_linear

.. autofunction:: find_empty_columns

.. autofunction:: apply_transformations

.. autofunction:: find_average_mix

.. autofunction:: get_iea_markets

Database Utilities
------------------

.. autofunction:: fix_unset_technosphere_and_production_exchange_locations

.. autofunction:: set_global_location_for_additional_datasets

.. autofunction:: remove_nones

.. autofunction:: link_fix_mismatched_locations

.. autofunction:: rename_locations

.. autofunction:: get_exchange_amounts

.. autodata:: unit_ei_to_bw

Double Counting
---------------

The issue of double counting only arises under certain circumstances.
In our case, it was due to the later use of the processed data in a
model for a sector-coupled energy system in Germany.
Thus, ``get_double_counted_process_keys`` is specific to German electricity
and heat production, but can easily be adapted.

.. autofunction:: get_double_counted_process_keys

.. autofunction:: set_dc_process_inputs_to_zero

Export
------

.. autofunction:: get_sign_mapping

.. autofunction:: excel_export
