Getting Started
===============

Required Software
-----------------
Python 3.3 or greater
   This code has been developed and tested with both
   Anaconda Python and CPython but will work under any Python implementation.

Packages
   * ``jupyter``
   * ``pandas``
   * ``brightway2``
   * ``wurst``
   * ``xlsxwriter``

Optional
^^^^^^^^
Jupytext
   We used jupytext_ to version control our code. If you downloaded the code
   directly from our repository, you will need to run

   .. code-block:: bash

      jupytext --sync *.pct.py

   in the main directory, to convert the script files into Jupyter Notebook,
   which you can the open in the web interface.
   ``functions_to_modify_ecoinvent.py`` can also be viewed as a Jupyter Notebook
   using jupytext.

   .. _jupytext: https://github.com/mwouts/jupytext

Datasets
--------

We need a variety of additional datasets
apart from the LCI already provided in the ``data`` directory.
Please also place all of these in the ``data`` directory or change the filepaths in
1_basic_setup_ and 2_creating_new_versions_ according to where you saved them.

Ecoinvent Dataset
   Our analysis was done using the ecoinvent 3.5 apos dataset.
   As discussed in our paper,
   our additional LCI datasets provided in ``data/lci-hydro.xlsx``
   are also based on this version.

   Additionally, you will need the cumulated matrices provided by ecoinvent for
   your respective version.

Technology Predictions
   We modify ecoinvent based on technology composition mix predictions made by
   the IEA in |IEA_Report|_.
   You will need to retrieve ``Scenario data data files (zip)``
   from the Downloads section of the linked webpage and unpack it,
   so that it can be processed in 1_basic_setup_.

Additional LCI
   As most of our research expands on |Mendoza_cit|_, we also need to import
   the LCI provided in the supporting information, downloadable from the linked
   webpage.

First Steps
-----------

First, we import the ecoinvent data into a Brightway2 database. All
the required steps are done in 1_basic_setup_.

.. _1_basic_setup: 1_basic_setup.ipynb
.. _2_creating_new_versions: 2_creating_new_versions.ipynb
