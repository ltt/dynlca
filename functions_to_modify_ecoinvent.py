# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.4.2
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %% [markdown]
# <div class="alert alert-warning">
#
# **Warning**
#
# While this file can be opened as a notebook via jupytext, it is optimized to be viewed as a python source file and is also documented as such. If you want to read through the documentation, it can be found here.
# </div>

# %% [markdown]
# # Prepare notebook and import useful libraries
# This file contains functions that will be used to modify ecoinvent.

# %%
from pprint import pformat
import warnings
import pandas as pd
import pyprind
import brightway2 as bw
import numpy as np
import xlsxwriter
import os
import json
import pprint
from datetime import datetime

# %% [markdown]
# We use the Wurst and constructive geometries libraries:

# %%
from wurst import *
from wurst.searching import *

from wurst.ecoinvent.electricity_markets import *
from wurst.ecoinvent.filters import *

# %% [markdown]
# Define the location of the IEA data here as well as an excel sheet that defines the IEA variable names that we will use (this file is provided too):

# %%
scenarios = {"2DS": {}}
#: path of the directory where scenatio data can be found
scenarios["2DS"]["filepath"] = "data/2°C Scenario/"
scenarios["2DS"]["description"] = "2°C Scenario International Energy Agency"

iea_variable_names = pd.read_excel("data/IEA variable names.xlsx")

# %% [markdown]
# Add the IEA regions to our known geographies and import the IEA topology:

# %%
REGIONS_IEA = iea_variable_names["Regions"].dropna().values

IEA_TOPOLOGY = json.load(open("data/iea-region-topolgy.json"))
geomatcher.add_definitions(IEA_TOPOLOGY, "IEA", relative=True)


# %% [markdown]
# # New Functions

# %%
def interpolate_linear(df):
    """Linearly interpolates data for all missing years in the dataframe.

    The years are assumed to be the index of the dataframe. The interpolation
    is done for all years between the earliest and the latest year present
    in the dataframe. Interpolated rows will be added to ``df``.

    Parameters
    ----------
    df : pandas.DataFrame
        DataFrame to interpolate rows of missing years for. Needs to be indexed by year.
    """
    years = df.index
    for int_year in np.arange(years.min(), years.max()):
        if int_year not in years:
            a = years.where(years < int_year).max()
            b = years.where(years > int_year).min()
            df.loc[int_year] = df.loc[a] + (int_year - a) * (df.loc[b] - df.loc[a]) / (
                b - a
            )


# %%
def find_empty_columns(df):
    """Searches through a dataframe for the names of all columns that are empty.

    Parameters
    ----------
    df: DataFrame
        DataFrame to calculate the empty columns for.
    """
    drop_list = []
    for col in df.columns:
        if df[col].sum() == 0:
            drop_list.append(col)
    return drop_list


# %% [markdown]
# ## Modify Carma Datasets
#
# Carma datasets are CCS datasets taken from project Carma - see Volkart 2013.

# %%
carma_electricity_ds_name_dict = {
    "Electricity, at wood burning power plant 20 MW, truck 25km, post, pipeline 200km, storage 1000m/2025": "Biomass CCS",
    "Electricity, at power plant/natural gas, NGCC, no CCS/2025/kWh": "Natural gas CC",
    "Electricity, at power plant/natural gas, pre, pipeline 400km, storage 3000m/2025": "Natural gas CCS",
    "Electricity, at BIGCC power plant 450MW, pre, pipeline 200km, storage 1000m/2025": "Biomass CCS",
    "Electricity, at power plant/hard coal, PC, no CCS/2025": "Coal ST",
    "Electricity, at power plant/hard coal, IGCC, no CCS/2025": "IGCC",
    "Electricity, at wood burning power plant 20 MW, truck 25km, no CCS/2025": "Biomass ST",
    "Electricity, at power plant/natural gas, pre, pipeline 200km, storage 1000m/2025": "Natural gas CCS",
    "Electricity, at power plant/lignite, PC, no CCS/2025": "Coal ST",
    "Electricity, at power plant/hard coal, pre, pipeline 200km, storage 1000m/2025": "Coal CCS",
    "Electricity, from CC plant, 100% SNG, truck 25km, post, pipeline 200km, storage 1000m/2025": "Biomass CCS",
    "Electricity, at wood burning power plant 20 MW, truck 25km, post, pipeline 400km, storage 3000m/2025": "Biomass CCS",
    "Electricity, at power plant/hard coal, oxy, pipeline 400km, storage 3000m/2025": "Coal CCS",
    "Electricity, at power plant/lignite, oxy, pipeline 200km, storage 1000m/2025": "Coal CCS",
    "Electricity, at power plant/hard coal, post, pipeline 400km, storage 3000m/2025": "Coal CCS",
    "Electricity, at power plant/lignite, pre, pipeline 200km, storage 1000m/2025": "Coal CCS",
    "Electricity, at BIGCC power plant 450MW, pre, pipeline 400km, storage 3000m/2025": "Biomass CCS",
    "Electricity, at power plant/natural gas, post, pipeline 400km, storage 1000m/2025": "Natural gas CCS",
    "Electricity, at power plant/lignite, post, pipeline 400km, storage 3000m/2025": "Coal CCS",
    "Electricity, at power plant/hard coal, post, pipeline 400km, storage 1000m/2025": "Coal CCS",
    "Electricity, from CC plant, 100% SNG, truck 25km, post, pipeline 400km, storage 3000m/2025": "Biomass CCS",
    "Electricity, at power plant/natural gas, ATR H2-CC, no CCS/2025": "Natural gas CCS",
    "Electricity, at power plant/hard coal, pre, pipeline 400km, storage 3000m/2025": "Coal CCS",
    "Electricity, at power plant/lignite, IGCC, no CCS/2025": "IGCC",
    "Electricity, at power plant/hard coal, post, pipeline 200km, storage 1000m/2025": "Coal CCS",
    "Electricity, at power plant/lignite, oxy, pipeline 400km, storage 3000m/2025": "Coal CCS",
    "Electricity, at power plant/lignite, post, pipeline 200km, storage 1000m/2025": "Coal CCS",
    "Electricity, at power plant/lignite, pre, pipeline 400km, storage 3000m/2025": "Coal CCS",
    "Electricity, at power plant/natural gas, post, pipeline 200km, storage 1000m/2025": "Natural gas CCS",
    "Electricity, at power plant/natural gas, post, pipeline 400km, storage 3000m/2025": "Natural gas CCS",
    "Electricity, at BIGCC power plant 450MW, no CCS/2025": "Biomass ST",
    "Electricity, from CC plant, 100% SNG, truck 25km, no CCS/2025": "Biomass ST",
    "Electricity, at power plant/hard coal, oxy, pipeline 200km, storage 1000m/2025": "Coal CCS",
}


# %% [markdown]
# # Functions to clean up non-ecoinvent datasets:
#
# We import some datasets from simapro. These functions clean up the datasets:

# %%
def fix_unset_technosphere_and_production_exchange_locations(
    db, matching_fields=("name", "unit")
):
    """Fills in all missing location fields for technosphere and production exchanges.

    For production exchanges the location is set to the location of the activity which the exchange is part of.
    For technosphere exchanges, loactions are instead set to the location of another activity
    matching the exchange. If there is not exactly one such activity, the location will not be changed.

    Parameters
    ----------
    db
        Database to fix locations for. Expected to be in wurst format.
    matching_fields: iterable
        Names of the fields that technosphere exchanges and activities in the database are matched on.
    """
    for ds in db:
        for exc in ds["exchanges"]:
            if exc["type"] == "production" and exc.get("location") is None:
                exc["location"] = ds["location"]
            elif exc["type"] == "technosphere" and exc.get("location") is None:
                locs = _find_location_given_lookup_dict(
                    db, {k: exc.get(k) for k in matching_fields}
                )
                if len(locs) == 1:
                    exc["location"] = locs[0]
                else:
                    print(
                        "No unique location found for exchange:\n{}\nFound: {}".format(
                            pprint.pformat(exc), locs
                        )
                    )


def _find_location_given_lookup_dict(db, lookup_dict):
    return [
        x["location"]
        for x in get_many(db, *[equals(k, v) for k, v in lookup_dict.items()])
    ]


# %%
exists = lambda x: {k: v for k, v in x.items() if v is not None}


def remove_nones(db):
    """Removes all fields of all exchanges whose value is ``None``.

    Parameters
    ----------
    db
        Database to remove empty exchange fields for. Expected to be in wurst format.
    """
    for ds in db:
        ds["exchanges"] = [exists(exc) for exc in ds["exchanges"]]


# %%
def set_global_location_for_additional_datasets(db, ei_name):
    """Set the location of all added datasets to the global location (``GLO``).

    For this function, we assume that all datasets, whose database code is not ``ei_name``, are externally added.
    All added datasets and all exchanges of the same name will have their location changed to ``GLO`` .

    This function is needed because the wurst function relink_technosphere exchanges needs global datasets if it cannot find a regional one.


    Parameters
    ----------
    db
        Database to change locations on. Expected to be in wurst format.
    ei_name : str
        Name of the ecoinvent database. Activities from this database will not have their locations changed.
    """
    non_ecoinvent_datasets = [x["name"] for x in db if x["database"] != ei_name]
    ecoinvent_datasets = [x["name"] for x in db if x["database"] == ei_name]
    print(
        "Found {} ecoinvent datasets and {} non-ecoinvent datasets.".format(
            len(ecoinvent_datasets), len(non_ecoinvent_datasets)
        )
    )

    for ds in [x for x in db if x["database"] != ei_name]:
        ds["location"] = "GLO"
        for exc in [x for x in ds["exchanges"] if x["type"] != "biosphere"]:
            if exc["name"] in non_ecoinvent_datasets:
                if exc["name"] in ecoinvent_datasets and exc["location"] != "GLO":
                    print(exc["name"], exc["location"])
                    print("here")
                else:
                    exc["location"] = "GLO"
                    # print("Set location to 'GLO' for exchange: {}".format(exc['name']))


# %%
def add_new_locations_to_added_datasets_iea(db):
    """Creates copies of added electricity activities for IEA locations.

    For each electricity activity, only one localised activity will be created.


    Parameters
    ----------
    db
        Database to add localised activities to. Expected to be in wurst format.


    See Also
    --------
    regionalize_added_datasets : Regionalize exchanges added using this function.
    """
    # We create a new version of all added electricity generation datasets for each IEA region.
    # We allow the upstream production to remain global, as we are mostly interested in regionalizing
    # to take advantage of the regionalized IEA data.

    # step 1: make copies of all datasets for new locations
    # best would be to regionalize datasets for every location with an electricity market like this:
    # locations = {x['location'] for x in get_many(db, *electricity_market_filter_high_voltage)}
    # but this takes quite a long time. For now, we just use 1 location that is unique in each IEA region.
    possibles = {}
    for reg in REGIONS_IEA[:-1]:
        print(reg)
        temp = [x for x in geomatcher.intersects(("IEA", reg)) if type(x) != tuple]
        possibles[reg] = [x for x in temp if len(ecoinvent_to_iea_locations(x)) == 1]
        if not len(possibles[reg]):
            print(reg, " has no good candidate")
    locations = [v[0] for v in possibles.values()]
    print(locations)

    # This code would modify every new dataset, but this would be quite large:
    # for ds in  pyprind.prog_bar([ds for ds in db if ds['database'] in ['CSP','Carma CCS']]):
    # so we consider only the final electricity production dataset and not the upstream impacts:
    for ds in pyprind.prog_bar(
        [ds for ds in db if ds["name"] in carma_electricity_ds_name_dict.keys()]
    ):
        for location in locations:
            new_ds = copy_to_new_location(ds, location)
            db.append(new_ds)


def regionalize_added_datasets(db):
    """Links exchanges of added electricity activities to regionalized activities.

    Parameters
    ----------
    db
        Database to regionalize activities for. Expected to be in wurst format.
    """
    # step 2: relink all processes in each dataset
    # This code would modify every new dataset, but this would be quite large:
    # for ds in  pyprind.prog_bar([ds for ds in db if ds['database'] in ['CSP','Carma CCS']]):
    # so we consider only the final electricity production dataset and not the upstream impacts:
    for ds in [ds for ds in db if ds["name"] in carma_electricity_ds_name_dict.keys()]:
        ds = relink_technosphere_exchanges(
            ds,
            db,
            exclusive=True,
            drop_invalid=False,
            biggest_first=False,
            contained=False,
        )


# %%
def link_fix_mismatched_locations(
    data, fields=("name", "product", "location", "unit"), warn_unsafe=False
):
    """Links technosphere exchanges by ``fields`` and fix mismatched locations.

    This function is an extension of wursts own `link_internal`_ function.
    Exchanges ``input`` values are set to ("linked") an activity matching all supplied ``fields``.
    If no direct match can be found, the search is expanded to also accept other
    location codes, searching for global region (``GLO``) first and
    rest of the world (``RoW``) afterwards.


    Parameters
    ----------
    data:
        Database to link the locations for. Expected to be in wurst format.
    fields: iterable
        Names of all fields that have to match between exchange
        and product for them to get linked. Required to contain ``"location"``.
    warn_unsafe: bool
        Whether to output a warning, if an exchange is not linked using the
        exact location.

    Warns
    -----
    UserWarning
        Can be activated with the ``warn_unsafe`` parameter, to issue a warning when
        an exchange is linked based on the substituted location codes.
        That warning will show the exchange and its newly linked target.
        If other matches exist, they will be listed as well.


    .. _link_internal:
        https://wurst.readthedocs.io/technical.html#wurst.linking.link_internal"""
    # helper functions to shorten common tuple operations
    get_tuple = lambda exc: tuple([exc[f] for f in fields])

    def change_loc(tpl, new_loc):
        # if a ValueError is raised here, you should be using link_internal directly
        i = fields.index("location")
        if new_loc == None:
            return tpl[:i] + tpl[i + 1 :]
        else:
            return tpl[:i] + (new_loc,) + tpl[i + 1 :]

    products = {
        get_tuple(reference_product(ds)): (ds["database"], ds["code"]) for ds in data
    }

    for ds in data:
        for exc in ds["exchanges"]:
            if exc.get("input"):
                continue

            if exc["type"] == "biosphere":
                raise ValueError(
                    "Unlinked biosphere exchange: \n{}".format(pformat(exc))
                )

            exc_key = get_tuple(exc)
            exc_matches = [exc_key] + [
                change_loc(exc_key, loc) for loc in ["GLO", "RoW"]
            ]
            try:
                key = next(e for e in exc_matches if e in products)
                exc["input"] = products[key]
            except StopIteration:
                # No Match found, try matches in any location
                exc_links = [
                    v
                    for k, v in products.items()
                    if change_loc(k, None) == change_loc(exc_key, None)
                ]
                if len(exc_links) == 0:
                    raise KeyError(
                        "Can't find linking activity for exchange:\n{}".format(
                            pformat(exc)
                        )
                    )
                else:
                    exc_links = sorted(exc_links)
                    exc["input"] = exc_links[0]

                    if warn_unsafe:
                        # Warn that the match may not quite be what you want it to be
                        get_exc = lambda x: tuple(
                            get_one(data, equals("code", x[1]))[f]
                            for f in ["name", "reference product", "location", "unit"]
                        )
                        msg = (
                            "Exchange {} from dataset {} was linked to {}. \n"
                            "This was the closest match found, but may still "
                            "lead to undesired "
                            "results."
                        ).format(exc_key, ds["database"], get_exc(exc_links[0]))
                        if len(exc_links) > 1:
                            msg += ("\nOther possible matches are:\n" " - {}").format(
                                "\n - ".join([str(get_exc(s)) for s in exc_links[1:6]])
                            )
                        if len(exc_links) > 6:
                            msg += "and {} more.".format(len(exc_links) - 6)
                        warnings.warn(msg)
    return data


# %% [markdown]
# # Modify electricity datasets

# %%
def get_exchange_amounts(ds, technosphere_filters=None, biosphere_filters=None):
    """Gets exchange amounts of ``ds`` exchanges. Optionally filter returned exchanges.

    Parameters
    ----------
    ds
        Activity to get exchange amounts from. Expected to be in wurst format.
    technosphere_filters , biosphere_filters
        Filter returned technosphere or biosphere exchanges respectively.
        Use the functions from wurst.searching_.


    .. _wurst.searching:
        https://wurst.readthedocs.io/technical.html#searching

    Returns
    -------
    dict
        The keys are tuples containing the name and location for a
        technosphere exchange or the name and categories for a
        biosphere exchange. The value is the amount of the
        respective exchange.
    """
    result = {}
    for exc in technosphere(ds, *(technosphere_filters or [])):
        result[(exc["name"], exc["location"])] = exc["amount"]
    for exc in biosphere(ds, *(biosphere_filters or [])):
        result[(exc["name"], exc["categories"])] = exc["amount"]
    return result


# %%
no_al = [exclude(contains("name", "aluminium industry"))]
no_ccs = [exclude(contains("name", "carbon capture and storage"))]
no_markets = [exclude(contains("name", "market"))]
no_imports = [exclude(contains("name", "import"))]
generic_excludes = no_al + no_ccs + no_markets

# %% [markdown]
# # Electricity markets:
# ## Define available technologies:

# %%
available_electricity_generating_technologies_iea = {
    "Solar PV": [
        "electricity production, photovoltaic, 3kWp slanted-roof installation, multi-Si, panel, mounted",
        "electricity production, photovoltaic, 3kWp slanted-roof installation, single-Si, panel, mounted",
        "electricity production, photovoltaic, 570kWp open ground installation, multi-Si",
    ],
    "Solar CSP": [
        "Electricity production for a 50MW parabolic trough power plant",  # Will be available in ecoinvent 3.4
        "Electricity production at a 20MW solar tower power plant",
    ],  # Will be available in ecoinvent 3.4
    "Wind onshore": [
        "electricity production, wind, <1MW turbine, onshore",
        "electricity production, wind, 1-3MW turbine, onshore",
        "electricity production, wind, >3MW turbine, onshore",
    ],
    "Wind offshore": ["electricity production, wind, 1-3MW turbine, offshore"],
    "Hydro (excl. pumped storage)": [
        "electricity production, hydro, reservoir, alpine region",
        "electricity production, hydro, reservoir, non-alpine region",
        "electricity production, hydro, reservoir, tropical region",
        "electricity production, hydro, run-of-river",
    ],
    "Nuclear": [
        "electricity production, nuclear, boiling water reactor",
        "electricity production, nuclear, pressure water reactor, heavy water moderated",
        "electricity production, nuclear, pressure water reactor",
    ],
    "Oil": ["electricity production, oil", "heat and power co-generation, oil"],
    "Coal": [
        "electricity production, hard coal",
        "electricity production, lignite",
        "heat and power co-generation, hard coal",
        "heat and power co-generation, lignite",
        "Electricity, at power plant/hard coal, IGCC, no CCS/2025",
        "Electricity, at power plant/lignite, IGCC, no CCS/2025",
    ],
    "Coal with CCS": [
        "Electricity, at power plant/hard coal, pre, pipeline 200km, storage 1000m/2025",
        "Electricity, at power plant/lignite, pre, pipeline 200km, storage 1000m/2025",
        "Electricity, at power plant/hard coal, post, pipeline 200km, storage 1000m/2025",
        "Electricity, at power plant/lignite, post, pipeline 200km, storage 1000m/2025",
        "Electricity, at power plant/lignite, oxy, pipeline 200km, storage 1000m/2025",
        "Electricity, at power plant/hard coal, oxy, pipeline 200km, storage 1000m/2025",
    ],
    "Natural gas": [
        "electricity production, natural gas, conventional power plant",
        "electricity production, natural gas, combined cycle power plant",
        "heat and power co-generation, natural gas, combined cycle power plant, 400MW electrical",
        "heat and power co-generation, natural gas, conventional power plant, 100MW electrical",
    ],
    "Natural gas with CCS": [
        "Electricity, at power plant/natural gas, pre, pipeline 200km, storage 1000m/2025",
        "Electricity, at power plant/natural gas, post, pipeline 200km, storage 1000m/2025",
    ],
    "Biomass and waste": [
        "heat and power co-generation, wood chips, 6667 kW, state-of-the-art 2014",
        "heat and power co-generation, wood chips, 6667 kW",
        "heat and power co-generation, biogas, gas engine",
    ],
    "Biomass with CCS": [
        "Electricity, from CC plant, 100% SNG, truck 25km, post, pipeline 200km, storage 1000m/2025",
        "Electricity, at wood burning power plant 20 MW, truck 25km, post, pipeline 200km, storage 1000m/2025",
        "Electricity, at BIGCC power plant 450MW, pre, pipeline 200km, storage 1000m/2025",
    ],
    "Geothermal": ["electricity production, deep geothermal"],
    "Ocean": [
        "electricity production at 1MW wave energy converter",
        "electricity production at 1MW tidal stream generator",
    ],
    "Other": [],
}

# %% [markdown]
# ## Overall function to change markets

# %%
# these locations aren't found correctly by the constructiive geometries library - we correct them here:
fix_names = {
    "CSG": "CN-CSG",
    "SGCC": "CN-SGCC",
    "RFC": "US-RFC",
    "SERC": "US-SERC",
    "TRE": "US-TRE",
    "ASCC": "US-ASCC",
    "HICC": "US-HICC",
    "FRCC": "US-FRCC",
    "SPP": "US-SPP",
    "MRO, US only": "US-MRO",
    "NPCC, US only": "US-NPCC",
    "WECC, US only": "US-WECC",
    "IAI Area, Africa": "IAI Area 1, Africa",
    "IAI Area, South America": "IAI Area 3, South America",
    "IAI Area, Asia, without China and GCC": "IAI Area 4&5, without China",
    "IAI Area, North America, without Quebec": "IAI Area 2, without Quebec",
    "IAI Area, Gulf Cooperation Council": "IAI Area 8, Gulf",
}

# %%
fix_names_back = {
    "IAI Area 1, Africa": "IAI Area, Africa",
    "IAI Area 3, South America": "IAI Area, South America",
    "IAI Area 4&5, without China": "IAI Area, Asia, without China and GCC",
    "IAI Area 2, without Quebec": "IAI Area, North America, without Quebec",
    "IAI Area 8, Gulf": "IAI Area, Gulf Cooperation Council",
}


# %%
def rename_locations(db, name_dict):
    """Renames locations in activities and technosphere exchanges according to ``name_dict``.

    Renaming the locations is necessary, because wurst and ecoinvent have differening naming
    conventions. There are two dictionaries supplied for most use cases of this function:
    ``fix_names`` and ``fix_names_back``. They handle the common naming inconsistencies.

    Parameters
    ----------
    db:
        Database to rename locations for. Expected to be in wurst format.
    name_dict: dict
        Dictionary specifying the renaming scheme.
        Locations will be renamed from ``key`` to ``value``.
    """
    for ds in db:
        if ds["location"] in name_dict.keys():
            ds["location"] = name_dict[ds["location"]]

        for exc in technosphere(ds):
            if exc["location"] in name_dict.keys():
                exc["location"] = name_dict[exc["location"]]


# %% [markdown]
# Function that returns all IEA locations that are interested by an ecoinvent location:

# %%
def ecoinvent_to_iea_locations(loc):
    """Finds IEA locations matching ``loc``.

    Parameters
    ----------
    loc : str
        Ecoinvent location code to find IEA locations for.

    Returns
    -------
    list
        List of all IEA locations geographically intersecting in ``loc``.
    """
    if loc == "RoW":
        loc = "GLO"

    if loc in fix_names:
        new_loc_name = fix_names[loc]
        return [r[1] for r in geomatcher.intersects(new_loc_name) if r[0] == "IEA"]
    else:
        return [r[1] for r in geomatcher.intersects(loc) if r[0] == "IEA"]


# %%
def update_electricity_markets_iea(db, year, scenario):
    """Replaces electricity market exchanges with the added activities and modify these using a scenario.

    Parameters
    ----------
    db
        Database to update markets for. Expected to be in wurst format.
    year : int
        Year to get the scaling factors for.
    scenario : str
        Scenario's name. Must already be present in the ``scenarios`` dict.


    Returns
    -------
    changes : dict
        Dictionary documenting what was changed in the database.
    """
    iea_electricity_market_df = get_iea_markets(year, scenario)

    # Remove all electricity producers from markets:
    db = empty_low_voltage_markets(db)
    db = empty_medium_voltage_markets(db)
    db = empty_high_voltage_markets(
        db
    )  # This function isn't working as expected - it needs to delete imports as well.

    changes = {}
    for ds in get_many(db, *electricity_market_filter_high_voltage):
        changes[ds["code"]] = {}
        changes[ds["code"]].update(
            {("meta data", x): ds[x] for x in ["name", "location"]}
        )
        changes[ds["code"]].update(
            {("original exchanges", k): v for k, v in get_exchange_amounts(ds).items()}
        )
        delete_electricity_inputs_from_market(
            ds
        )  # This function will delete the markets. Once Wurst is updated this can be deleted.
        add_new_datasets_to_electricity_market_iea(ds, db, iea_electricity_market_df)
        changes[ds["code"]].update(
            {("updated exchanges", k): v for k, v in get_exchange_amounts(ds).items()}
        )
    return changes


# %% [markdown]
# ## Define electricity market filters

# %%
electricity_market_filter_high_voltage = [
    contains("name", "market for electricity, high voltage"),
    doesnt_contain_any("name", ["aluminium industry", "internal use in coal mining"]),
]


# %% [markdown]
# ## Modify high voltage markets

# %%
def delete_electricity_inputs_from_market(ds):
    """Deletes all electricity market exchanges from a process.

    Parameters
    ----------
    ds
        Process to delete market exchanges from.
    """
    # This function reads through an electricity market dataset and deletes all electricity inputs that are not own consumption.
    ds["exchanges"] = [
        exc
        for exc in get_many(
            ds["exchanges"],
            *[
                either(
                    *[
                        exclude(contains("unit", "kilowatt hour")),
                        contains("name", "market for electricity, high voltage"),
                        contains("name", "market for electricity, medium voltage"),
                        contains("name", "market for electricity, low voltage"),
                        contains("name", "electricity voltage transformation"),
                    ]
                )
            ]
        )
    ]


# %%
def find_average_mix(df):
    """Gets the average mix across all regions from a scenario DataFrame.

    Parameters
    ----------
    df : pandas.DataFrame
        Dataframe containing the scenario data.

    Returns
    -------
    pandas.Series
        Series lisiting technologies and their percentage of the scenario's total mix.
    """
    # This function considers that there might be several IEA regions that match the ecoinvent region.
    return df.divide(df.sum().sum()).sum()


# %%
def get_iea_markets(year, scenario):
    """Gets the scenario's predicted technology composition for the specified year.

    Parameters
    ---------
    year : int
        Year to get the technology composition for.
    scenario : str
        Scenario's name. Must already be present in the ``scenarios`` dict.
    """
    fp = os.path.join(
        scenarios[scenario]["filepath"], "Gross electricity generation (TWh).xlsx"
    )

    dfs = pd.read_excel(fp, sheet_name=None, index_col=0)

    for df in dfs.values():
        interpolate_linear(df)

    df = pd.concat(
        [pd.Series(df.loc[year], name=region) for region, df in dfs.items()], axis=1
    )

    empty_columns = find_empty_columns(df)
    df = df.divide(df.sum(axis=0)).T.drop(empty_columns, axis=1)

    return df


# %%
def find_ecoinvent_elec_ds_in_same_ecoinvent_location_iea(tech, location, db):
    """Finds electricity activities for `tech` that are in `location`.

    For new datasets, the corresponding IEA region will be used as the location.

    Parameters
    ----------
    tech : str
        Name of the technology to be found.
        One of the keys from ``available_electricity_generating_technologies_iea``.
    location : str
        Location of the activity to be found.
    db
        Database to search for electricity activities. Expected to be in wurst format.
    """
    try:
        return [
            x
            for x in get_many(
                db,
                *[
                    either(
                        *[
                            equals("name", name)
                            for name in available_electricity_generating_technologies_iea[
                                tech
                            ]
                        ]
                    ),
                    equals("location", location),
                    equals("unit", "kilowatt hour"),
                ]
            )
        ]
    except:
        return [
            x
            for x in get_many(
                db,
                *[
                    either(
                        *[
                            equals("name", name)
                            for name in available_electricity_generating_technologies_iea[
                                tech
                            ]
                        ]
                    ),
                    equals("location", ecoinvent_to_iea_locations(location)),
                    equals("unit", "kilowatt hour"),
                ]
            )
        ]


# %%
def find_other_ecoinvent_regions_in_iea_region(loc):
    """Finds all ecoinvent regions that are geographically intersecting the given IEA region.

    Parameters
    ----------
    loc : str
        Location to get ecoinvent locations for.
    """
    if loc == "RoW":
        loc = "GLO"

    if loc in fix_names:
        new_loc_name = fix_names[loc]
        iea_regions = [r for r in geomatcher.intersects(new_loc_name) if r[0] == "IEA"]

    else:
        iea_regions = [r for r in geomatcher.intersects(loc) if r[0] == "IEA"]

    temp = []
    for iea_region in iea_regions:
        temp.extend(
            [
                r
                for r in geomatcher.contained(iea_region)
                if r[0] == "ecoinvent" or type(r) != tuple
            ]
        )  ##HM added if so that online ecoinvent regions are found TODO

    result = []
    for temp in temp:
        if type(temp) == tuple:
            result.append(temp[1])
        else:
            result.append(temp)
    return set(result)


# %%
def find_ecoinvent_elec_ds_in_iea_region(tech, region, db):
    """Finds electricity activities for the given technology that are in the same IEA region.

    Parameters
    ----------
    tech : str
        Name of the technology to be found.
        One of the keys from ``available_electricity_generating_technologies_iea``.
    region : str
        Region of the activity to be found.
    db
        Database to search for electricity activities. Expected to be in wurst format.
    """
    return [
        x
        for x in get_many(
            db,
            *[
                either(
                    *[
                        equals("name", name)
                        for name in available_electricity_generating_technologies_iea[
                            tech
                        ]
                    ]
                ),
                either(
                    *[
                        equals("location", loc)
                        for loc in find_other_ecoinvent_regions_in_iea_region(region)
                    ]
                ),
                equals("unit", "kilowatt hour"),
            ]
        )
    ]


# %%
def find_ecoinvent_elec_ds_in_all_locations_iea(tech, db):
    """Finds electricity activities for the given technology in all locations.

    Parameters
    ----------
    tech : str
        Name of the technology to be found.
        One of the keys from ``available_electricity_generating_technologies_iea``.
    db
        Database to search for electricity activities. Expected to be in wurst format.
    """
    return [
        x
        for x in get_many(
            db,
            *[
                either(
                    *[
                        equals("name", name)
                        for name in available_electricity_generating_technologies_iea[
                            tech
                        ]
                    ]
                ),
                equals("unit", "kilowatt hour"),
            ]
        )
    ]


# %%
def add_new_datasets_to_electricity_market_iea(ds, db, df):
    """Adds new electricity activities as exchanges to `ds` based on IEA results.

    1. Get mix for the activity's location.
       If the dataset location contains multiple IEA regions, the average mix of
       those IEA regions is used.
       If no ecoinvent location matches, the worldwide scenario will be used instead.
    2. For all technologies, find the corresponding electricity generating activities.
       These will be searched for in the following order:

        1. In the same ecoinvent location (or IEA region for activities from other sources).
        2. In another ecoinvent location within the same IEA region.
        3. In the ``global`` region.
        4. In any location.

       If no match is found using the above methods,
       a search for global ``market group for electricity, high voltage``
       is conducted on `db` instead and a warning is printed.
    3. Add all found activities to `ds`' exchanges.
       The exchanges are added to `ds` with their amount equal to the amount specified
       for the respective technology in `df`, divided by the amount of exchanges added.
    4. Ensure that all exchange amounts add up to 100%.


    Parameters
    ----------
    ds
        Activity to modify. Expected to be in wurst format.
    db
        Database containing the exchanges to be considered for the passed activity `ds`.
        Expected to be in wurst format.
    df : pandas.DataFrame
        DataFrame containing the electricity mix information.
    """
    iea_locations = ecoinvent_to_iea_locations(ds["location"])
    if iea_locations:
        mix = find_average_mix(df.loc[iea_locations])
    else:
        print(
            "No matching IEA region found for location: {}. "
            "Taking Worldwide scenario instead.".format(ds["location"])
        )
        mix = find_average_mix(df.loc[["World"]])

    datasets = {}
    for i in mix.index:
        if mix[i] != 0:
            datasets[i] = find_ecoinvent_elec_ds_in_same_ecoinvent_location_iea(
                i, ds["location"], db
            )

            if len(datasets[i]) == 0:
                datasets[i] = find_ecoinvent_elec_ds_in_iea_region(
                    i, ds["location"], db
                )

            if len(datasets[i]) == 0:
                datasets[i] = find_ecoinvent_elec_ds_in_same_ecoinvent_location_iea(
                    i, "GLO", db
                )

            if len(datasets[i]) == 0:
                datasets[i] = find_ecoinvent_elec_ds_in_all_locations_iea(i, db)

            if len(datasets[i]) == 0:
                print(
                    "No match found for location: ",
                    ds["location"],
                    " Technology: ",
                    i,
                    ". Taking global market group for electricity",
                )
                datasets[i] = [
                    x
                    for x in get_many(
                        db,
                        *[
                            equals(
                                "name", "market group for electricity, high voltage"
                            ),
                            equals("location", "GLO"),
                        ]
                    )
                ]

    for i in mix.index:
        if mix[i] != 0:
            total_amount = mix[i]
            amount = total_amount / len(datasets[i])
            ## HM: equal distribution of electricity generation between different processes of one technology
            # (eg 3kWp solar PV and 570kWp solar PV). this also applies regionally
            # if no process from the same region can be identified.
            # eg: no process for hydro in France --> all hydro processes in the EU are found
            # and contribute equally to the french market
            for dataset in datasets[i]:
                ds["exchanges"].append(
                    {
                        "amount": amount,
                        "unit": dataset["unit"],
                        "input": (dataset["database"], dataset["code"]),
                        "type": "technosphere",
                        "name": dataset["name"],
                        "location": dataset["location"],
                        "product": dataset["reference product"],
                    }
                )

    sum = np.sum(
        [
            exc["amount"]
            for exc in technosphere(
                ds,
                *[
                    equals("unit", "kilowatt hour"),
                    doesnt_contain_any(
                        "name", ["market for electricity, high voltage"]
                    ),
                ]
            )
        ]
    )
    if round(sum, 4) != 1.00:
        print(
            ds["location"],
            " New exchanges don't add up to one! Something is wrong!",
            sum,
        )


# %% [markdown]
# # Solving double counting issues
#
# Define filters for processes which are double counted.

# %%
electricity_DE_filter = [
    contains("reference product", "electricity"),
    equals("location", "DE"),
]

heat_DE_filter = [
    contains("reference product", "heat,"),
    equals("location", "DE"),
    exclude(contains("reference product", "wheat")),
]

filter_liste = [electricity_DE_filter, heat_DE_filter]


# %%
def get_double_counted_process_keys(db):
    """Creates a list of all activities that get counted twice.

    Parameters
    ----------
    db
        Database containing double counted activities. Expected to be in wurst format.


    Returns
    -------
    dc_list : list
        List of all activities affected by double counting.
    """
    dc_list = []

    for flt in filter_liste:
        for ds in get_many(db, *flt):
            dc_list.append(ds)

    dc_key_list = [(ds["database"], ds["code"]) for ds in dc_list]
    return dc_list, dc_key_list


# %%
def set_dc_process_inputs_to_zero(db):
    """Sets amount of exchanges with double counted activities to zero.

    Parameters
    ----------
    db
        Database to fix double counting for. Expected to be in wurst format.
    """
    dc_liste, _ = get_double_counted_process_keys(db)
    for ds in db:
        for exc in ds["exchanges"]:
            for act in dc_liste:
                if exc["name"] == act["name"]:
                    if (
                        exc["location"] == act["location"]
                        and exc["product"] == act["reference product"]
                        and exc["unit"] == act["unit"]
                    ):
                        # only set technosphere flows to zero, not production flows (because that would cause empty rows in technosphere matrix - lci would not be solvable)
                        if exc["type"] == "technosphere":
                            exc["amount"] = 0


# %% [markdown]
# # Utilities

# %%
def apply_transformations(
    db, scenario, year=None, change_elec=False, change_dc=False,
):
    """Applies specified transformations to dataset.

    Meta method to facilitate creation of multiple databases.
    Internally calls ``update_electricity_markets_iea`` if ``change_elec = True``
    and ``set_dc_process_inputs_to_zero`` if ``change_dc = True``.

    Parameters
    ----------
    db
        Database to apply transformations to. Expected to be in wurst format.
    scenario : str
        Scenario's name. Must already be present in the ``scenarios`` dict.
    year : int, optional
        Year to get the scenario for. Needs to be specified if ``change_elec`` is ``True``.
    change_elec : bool, optional
        Whether to change electricity markets according to scenario.
    change_dc : bool, optional
        Whether to fix `double counting`_.
    """
    if change_elec:
        print("Changing electricity markets")
        update_electricity_markets_iea(db, year, scenario)

    if change_dc:
        print("Setting inputs of double counting processes to zero")
        set_dc_process_inputs_to_zero(db)


# %%
#: Dictionary mapping the unit shorthands used by ecoinvent to
#: the full unit names used by Brightway2.
unit_ei_to_bw = {
    "kg": "kilogram",
    "kWh": "kilowatt hour",
    "ha": "hectare",
    "unit": "unit",
    "m3": "cubic meter",
    "m*year": "meter-year",
    "MJ": "megajoule",
    "m": "meter",
    "tonnes*km": "ton kilometer",
    "m2": "square meter",
    "hour": "hour",
    "l": "liter",
    "km": "kilometer",
    "m2*year": "square meter-year",
    "kg*day": "kg*day",
    "km*year": "kilometer-year",
    "person*km": "person kilometer",
}


def get_sign_mapping():
    """Imports ecoinvent signs from an excel file.

    To see how these files are generated, have a look at 1_basic_setup.
    """
    signs = pd.read_excel("data/ecoinvent_signs.xlsx")
    return {tpl[:2]: tpl[2] for tpl in signs.itertuples(index=False)}


# %%
def excel_export(db_name):
    """Does an LCIA for all activities in the database and output the cumulated matrix as
    an excel file.

    The output is formatted in the same way ecoinvent's cumulated matrices are.

    Parameters
    ----------
    db_name : str
        Name of the Brightway2 database to calculate cumulated matrix for.
    """
    print(
        'Calculating LCA results for all processes in database "{}"...'.format(db_name)
    )
    database = bw.Database(db_name)
    Categories = [met for met in bw.methods if "cumulative energy demand" in str(met)]
    Categories += [
        met for met in bw.methods if "ILCD 2" in str(met) and "no LT" not in str(met)
    ]
    Categories += [
        met
        for met in bw.methods
        if "ReCiPe Midpoint (H)" in str(met)
        and "w/o" not in str(met)
        and "V1.13" not in str(met)
    ]

    calc_setup = {}
    calc_setup["inv"] = [{act: 1} for act in database]
    calc_setup["ia"] = Categories
    bw.calculation_setups["db_to_excel"] = calc_setup

    lca = bw.MultiLCA("db_to_excel")

    print("Finished the calculation.")

    results = lca.results

    print("Writing results (cumulated matrices) to Excel...")

    workbook = xlsxwriter.Workbook(db_name + ".xlsx")
    # for some units, ecoinvent uses different notations than Brightway. To avoid mistakes, dictionary {BW2:ecoinvent}
    change_unit_dict = {
        "square meter-year": "m2a",
        "square meter": "m2",
        "cubic meter": "m3",
    }

    worksheet = workbook.add_worksheet("Sheet1")
    col = 7
    for cat in [bw.Method(m) for m in Categories]:  # write headers
        if cat.metadata["unit"] in change_unit_dict:
            unit = change_unit_dict[cat.metadata["unit"]]
        else:
            unit = cat.metadata["unit"]

        worksheet.write(0, col, ":".join(cat.name))
        worksheet.write(2, col, unit)
        col += 1

    names = [
        act["reference product"] + "//[" + act["location"] + "] " + act["name"]
        for act in lca.all.keys()
    ]
    units = [act["unit"] for act in lca.all.keys()]
    sign_map = get_sign_mapping()
    signs = [sign_map.get(key, None) for key in zip(names, units)]

    worksheet.write_column(3, 1, names)
    worksheet.write_column(3, 2, units)
    worksheet.write_column(3, 3, signs)

    for row, data in enumerate(results):
        sign = signs[row]
        if sign == None:
            sign = 1
        worksheet.write_row(row + 3, 7, sign * data)

    workbook.close()

    print("Finished writing: " + datetime.now().strftime("%H:%M:%S"))
    print()
