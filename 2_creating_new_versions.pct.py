# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,.pct.py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.4.2
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Modify ecoinvent
#
# Here we do our main analysis, using [functions to modify ecoinvent](doc/functions_to_modify_ecoinvent.rst).
# The modified versions of ecoinvent are based on a dictionary of parameters.

# %% nbsphinx="hidden"
import brightway2 as bw
import pandas as pd
import copy

# %% [markdown]
# <div class="alert alert-info">
#
# Note
#
# The specified project needs to contain an ecoinvent database. Use the same project as in [1_basic_setup.ipynb](1_basic_setup.ipynb).
#
# </div>

# %%
bw.projects.set_current("your_project")

# %% [markdown]
# The functions we use to integrate scenarios in ecoinvent are based on the [wurst](https://github.com/polca/wurst) package
# and are documented in the ["Functions" section](doc/functions_to_modify_ecoinvent.rst).

# %%
from functions_to_modify_ecoinvent import *

# %% [markdown]
# ## Database selection
# After having a look at all available databases...

# %%
bw.databases

# %% [markdown]
# ... we choose an ecoinvent database as a basis for all our newly created ones.

# %%
ecoinvent_db_name = "ecoinvent3.5apos"

# %% [markdown]
# ## Prepare additional datasets
#
# The IEA scenarios that we implement into the database contain some technologies that
# lack corresponding datasets in the ecoinvent database.
# We modeled the missing datasets from literature sources and import them here.
# The literature sources are referred to in Reinert, C. et al. (2020).
#
# We now import all LCIs, that are not already present, from their respective excel files.

# %%
for k, fp in {
    "Carma CCS": "data/lci-Carma-CCS.xlsx",
    "CSP": "data/lci-CSP.xlsx",
    "Hydro": "data/lci-hydro.xlsx",
}.items():
    if k not in bw.databases:
        sp = bw.ExcelImporter(fp)
        sp.apply_strategies()
        sp.match_database(fields=["name", "unit", "location"])
        sp.match_database(
            ecoinvent_db_name, fields=["reference product", "name", "unit", "location"]
        )
        sp.match_database(ecoinvent_db_name, fields=["name", "unit", "location"])
        sp.write_database()

# %% [markdown]
# ## Import new datasets and ecoinvent into wurst format

# %%
input_db = extract_brightway2_databases(
    ["CSP", "Carma CCS", "Hydro", ecoinvent_db_name]
)

# %% [markdown]
# As some datasets don't have a location specified and some have unset exchange locations,
# we need to fix these inconsistencies. Additionally, we set the location of all new datasets
# to global (as we will later use [wurst](https://github.com/polca/wurst) functionality
# to regionalize some of the data and the function to regionalize activities requires them to have a global location code).

# %%
default_global_location(input_db)
fix_unset_technosphere_and_production_exchange_locations(input_db)
set_global_location_for_additional_datasets(input_db, ecoinvent_db_name)
remove_nones(input_db)

# %% [markdown]
# We are using the [constructive geometries library](https://github.com/cmutel/constructive_geometries)
# (by Chris Mutel). The library contains some naming inconsistencies, as has already been noted in
# [Mendoza Beltran et al. (2018)](https://onlinelibrary.wiley.com/doi/full/10.1111/jiec.12825), which we correct:

# %%
rename_locations(input_db, fix_names)

# %% [markdown]
# ## Create regional versions of additional datasets
# Our additional LCI's contain only global versions of each additional dataset.
# However, the IEA data that we are working with is regionalized.
# We use [wurst's](https://github.com/polca/wurst) functionality to make regional copies of the
# new datasets imported from excel and relink all exchanges with regional ones, where available.

# %%
add_new_locations_to_added_datasets_iea(input_db)

regionalize_added_datasets(input_db)

# %% [markdown]
# ## Define scenarios and years to be used
# Here, we set the scenarios and years we want to use when creating a new version of ecoinvent.
# Each entry in the ``database_dict`` will create a database named after the key with the parameters
# supplied as the value. Those parameters are documented within
# [apply transformation's parameters](doc/functions_to_modify_ecoinvent.rst#functions_to_modify_ecoinvent.apply_transformations),
# as we pass them to that function later on.
#
# <div class="alert alert-info">
#
# Note
#
# Adapt these parameters as needed to fit your own reasearch.
#
# </div>

# %%
database_dict = {}
database_dict["ei35_elec_dc_2016"] = {
    "year": 2016,
    "scenario": "2DS",
    "change_elec": True,
    "change_dc": True,
}
for year in range(2020, 2051, 5):
    database_dict["ei35_elec_dc_" + str(year)] = {
        "year": year,
        "scenario": "2DS",
        "change_elec": True,
        "change_dc": True,
    }


# %% [markdown]
# Now we do the main processing for every defined set of parameters:
#
# 1. Creating a copy of the database to perform the calculations on.
#
# 2. Applying all the transformations specified in the parameters dictionary to this database copy.
#
# 3. Deleting databases of the same name that are already present in our Brightway2 project,
#    as we assume these to be leftover from previous runs of the code.
#    <div class="alert alert-warning">
#
#     Make sure that this step does not delete important results!
#     </div>
#
# 4. Reverting all of the location naming changes that were necessary due to the constructive geometries library.
#
# 5. Fixing some possibly mismatched locations.
#
# 6. Exporting the new database to Brightway2.
#
# 7. Performing an LCA for all available processes and exporting to .xlsx for further use,
#    for example in our research as input of energy systems models.
#    If you just need the Brightway2 database, you can deactivate this step, as it is quite computationally intensive.
#

# %%
for db_name, parameters in database_dict.items():
    db = copy.deepcopy(input_db)

    apply_transformations(db, **parameters)

    ## delete db if existent
    if db_name in bw.databases:
        del bw.databases[db_name]

    ## export db
    rename_locations(db, fix_names_back)
    link_fix_mismatched_locations(db)
    write_brightway2_database(db, db_name)

    ## excel export
    excel_export(db_name)
