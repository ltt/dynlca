Code documentation
==============================================================

This code is part of the supporting information of |Reinert|, by |Reinert_cit|.
It is heavily based on the code supplied with |Mendoza|_, by |Mendoza_cit|.

To construct the modified ecoinvent databases from |Reinert_cit| we used
some additional LCI datasets, IEA predictions for future technology compositions
and the ecoinvent database itself. The preparation of the data is outlined
in `Basic Setup <1_basic_setup.ipynb>`_ and the process to modify ecoinvent
is documented in `Creating new Versions <2_creating_new_versions.ipynb>`_.

This documentation was last updated on 14.12.2020.

.. toctree::
   :caption: Contents:

   doc/getting_started
   Basic Setup <../1_basic_setup>
   Creating new versions <../2_creating_new_versions>
   doc/functions_to_modify_ecoinvent.rst
