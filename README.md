# Code documentation

Our documentation is available 
[here](https://ltt.pages.git-ce.rwth-aachen.de/vorkettenanalyse/).

Please cite our work as: C. Reinert, S. Deutz, H. Minten, L. Dörpinghaus, S. von Pfingsten, N. Baumgärtner, A. Bardow, Environmental Impacts of the Future German Energy System from Integrated Energy Systems Optimization and Dynamic Life Cycle Assessment, Computers and Chemical Engineering(2021), doi: https://doi.org/10.1016/j.compchemeng.2021.107406 .
